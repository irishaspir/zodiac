package com.zodiac.get.run;

import com.zodiac.get.logger.Logger;
import com.zodiac.get.utils.Timeout;
import org.apache.commons.lang.RandomStringUtils;

import java.io.*;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static com.zodiac.get.utils.WebConfigClient.*;
import static java.lang.Thread.sleep;

/**
 * Created by SpiridonovaIM on 29.08.2016.
 */
public class TCPClient implements Runnable {

    private String host = getURL();
    private int port = Integer.parseInt(getPORT());
    private static int lengthText = Integer.parseInt(getLengthText());
    private static int delay = Integer.parseInt(getDelayTime());
    private Socket socket = null;
    private static int amountMessage = 0;
    private static int threadCount = Integer.parseInt(getCountTread());

    public static void main(String args[]) throws Exception {

        ExecutorService executorService = Executors.newFixedThreadPool(threadCount);

        for (int i = 1; i <= threadCount; i++) {
            executorService.execute(new TCPClient());
        }
        executorService.shutdown();
    }

    public TCPClient() throws Exception {
        new Timeout(20000,"Stop Client");

    }

    private void sendToServer(String msg) throws Exception {
        socket.getOutputStream().write((msg + "\n").getBytes());
        incrementMessages();
    }

    private static synchronized void incrementMessages() {
        amountMessage++;
    }

    private String receiveFromServer() throws Exception {
        BufferedReader inFromServer = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        String res = inFromServer.readLine();
        return res;
    }

    private static void delay() {
        try {
            sleep(delay * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        while (true) {
            try {
                socket = new Socket(host, port);
                sendToServer(RandomStringUtils.randomAlphanumeric(lengthText));
                Logger.trace("Server said: " + receiveFromServer());
                delay();
            } catch (Exception ignore) {
                ignore.printStackTrace();
            }
        }
    }

    static {
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                Logger.info("Total sent messages: " + amountMessage);
            }
        });
    }

}

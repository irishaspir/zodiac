package com.zodiac.get.run;

import com.zodiac.get.logger.Logger;
import com.zodiac.get.utils.Timeout;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

import static com.zodiac.get.utils.WebConfigServer.getDelayTime;
import static com.zodiac.get.utils.WebConfigServer.getPORT;

/**
 * Created by SpiridonovaIM on 29.08.2016.
 */
public class TCPServer {

    public static int receivedMessages = 0;

    private int delayTime = Integer.parseInt(getDelayTime());
    private int port = Integer.parseInt(getPORT());

    public TCPServer() {
        new Timeout(30000, "Stop Server");
    }


    public static void main(String args[]) throws Exception {
        TCPServer server = new TCPServer();
        server.run();
    }

    public void run() throws Exception {
        ServerSocket welcomeSocket = new ServerSocket(port);

        while (true) {
            try {
                Socket connectionSocket = welcomeSocket.accept();
                Connection c = new Connection(connectionSocket);
                c.start();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    class Connection extends Thread {
        Socket connectionSocket;

        Connection(Socket _connectionSocket) {
            connectionSocket = _connectionSocket;
        }

        private void delay() {
            try {
                sleep(delayTime * 1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void run() {

            try {
                BufferedReader inFromClient = new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));
                PrintWriter outToClient = new PrintWriter(new OutputStreamWriter(connectionSocket.getOutputStream()));
                String clientSentence = inFromClient.readLine();
                receivedMessages++;
                Logger.trace("Client sent: " + clientSentence);
                delay();
                String capitalizedSentence = "Ok " + clientSentence + '\n';
                outToClient.print(capitalizedSentence);
                outToClient.flush();
            } catch (Exception e) {
            }
        }
    }


    static {
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                Logger.info("Total received messages: " + receivedMessages);
            }
        });
    }

}

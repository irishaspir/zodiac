package com.zodiac.get.run;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by SpiridonovaIM on 31.08.2016.
 */
public class TCPServerMy {

    public static void main(String[] args) throws IOException {

        ServerSocket servers = null;
        Socket fromclient = null;
        BufferedReader in = null;
        PrintWriter out = null;
        String input, output;

        System.out.println("Welcome to Server side");


        servers = new ServerSocket(4444);
        fromclient = servers.accept();

        in = new BufferedReader(new
                InputStreamReader(fromclient.getInputStream()));
        out = new PrintWriter(fromclient.getOutputStream(), true);


        System.out.println("Wait for messages");

        while ((input = in.readLine()) != null) {
            if (input.equalsIgnoreCase("exit")) break;
            out.println("S ::: " + input);
            System.out.println(input);

            out.close();
            in.close();
            fromclient.close();
            servers.close();

        }

    }

}


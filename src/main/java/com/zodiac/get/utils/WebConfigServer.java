package com.zodiac.get.utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by SpiridonovaIM on 29.08.2016.
 */
public class WebConfigServer {

    private static final String PORT = "server.port";
    private static final String URL = "server.ip";
    private static final String DELAY_TIME = "time.delay";
    private static final String propertiesFileName = "etc/config-server.properties";
    protected static Properties props = new Properties();
    private static InputStream is = WebConfigServer.class.getResourceAsStream(propertiesFileName);

    public static Properties readProperty() {

        try {
            is = new FileInputStream(propertiesFileName);
            props.load(is);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return props;
    }

    public static String getPORT() {
        readProperty();
        return props.getProperty(PORT);
    }

    public static String getURL() {
        readProperty();
        return props.getProperty(URL);
    }

    public static String getDelayTime() {
        readProperty();
        return props.getProperty(DELAY_TIME);
    }


}

package com.zodiac.get.utils;

import java.util.Timer;
import java.util.TimerTask;

import static java.lang.System.exit;
import static java.lang.System.out;

/**
 * Created by SpiridonovaIM on 06.09.2016.
 */
public class Timeout extends Timer {

    public Timeout(int delay, final String msg) {
        super(true);
        schedule(new TimerTask() {
            @Override
            public void run() {
                //    new Applet1().open();
                out.println(msg);
                exit(0);

            }
        }, delay);
    }
}

